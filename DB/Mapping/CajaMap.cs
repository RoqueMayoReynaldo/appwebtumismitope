﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTUMISMITOPE.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AppTUMISMITOPE.DB.Mapping
{
    public class CajaMap : IEntityTypeConfiguration<Caja>

    {
        public void Configure(EntityTypeBuilder<Caja> builder)
        {


            builder.ToTable("Caja", "dbo");
            builder.HasKey(Caja => Caja.id);


        }
    }
}
