﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppTUMISMITOPE.Models;
using AppTUMISMITOPE.DB;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace AppTUMISMITOPE.Controllers
{
    public class IndexModel
    {
        public List<Publicacion> pubs { get; set; }
        public Usuario UsuarioActivo { get; set; }
        public DateTime fechaActual { get; set; }
    }

    public class HomeController : Controller
    {

        private AppTumismitoPeContext context;
        private IWebHostEnvironment hosting;

    

        public HomeController(AppTumismitoPeContext context, IWebHostEnvironment hosting)
        {
            this.context = context;
            this.hosting = hosting;
           
        }


        /*
         estado 0 : estado inicial de una publkicacion (no cuenta con postulacion fijada)
        estado 1: publicacion que ya es dada por terminada por el trabajador,y adjuntada su evidencia
        estado 2 : cuenta con la postulacion ya fijada en funcion de los criterios del  tiempo establecido,likes,fecha pero aun no cumlmina la recaudacion
        estado 3: ya completo su monto y ya cuenta con un trabajdor,pero esta en espera de ser dado como culminado por el trabajador
         
         
         
         */



        /*
         pustulacion estados:

        estado 0: esta en votacion
        estado 1: fue elegido pero aun no completan monto
        estado 2: ya debe estar trabjaando
         
           estado 3: ya culmino
         
         */



        [HttpGet][Authorize]
        public IActionResult Index()
        {
            actualizarPostulaciones();

            var claim = HttpContext.User.Claims.First();
            string conectUser = claim.Value;


            IndexModel indexModel = new IndexModel
            {
                pubs = context.Publicaciones.Include("usuario").Include("fotos").Include("donaciones").Include("reacciones.usuario").Include("comentarios.usuario").Include("postulaciones.usuario.perfilProfesional").OrderByDescending(Publicacion => Publicacion.fechaCreacion).Where(o => o.estado == 0 || o.estado == 3 || o.estado==2 ).ToList(),
                UsuarioActivo = context.Usuarios.FirstOrDefault(User => User.userName == conectUser),
                fechaActual = DateTime.Now
            };
            
           return View("Index",indexModel);

        }

        [HttpPost]
        [Authorize]
        public IActionResult CrearPublicacion(Publicacion publicacion, List<IFormFile> files)
        {
            actualizarPostulaciones();

            publicacion.estado = 0;
            publicacion.fechaCreacion= DateTime.Now;
            publicacion.limitePostulaciones = DateTime.Parse("9999-12-31 01:00:00.000");
       
            context.Publicaciones.Add(publicacion);

            context.SaveChanges();
            int idPub = publicacion.id;
            foreach (IFormFile file in files)
            {
                Foto f = new Foto()
                {
                    detalle = file.FileName,
                    idPublicacion = idPub,
                    contenido = SaveFile(file).Replace(" ","%20")

                };
                context.Fotos.Add(f);
                context.SaveChanges();
            }
            return RedirectToAction("Index");

        }


    


        //deslogeo

        [HttpGet] [Authorize]
        public IActionResult Logout()
        {
            actualizarPostulaciones();

            HttpContext.SignOutAsync();

            return RedirectToAction("SingIn", "Welcome");
        }




        private string SaveFile(IFormFile file)
        {
            actualizarPostulaciones();

            string relativePath = "";

            if (file.Length > 0 && (file.ContentType == "image/png" || file.ContentType == "image/jpeg"))
            {
                relativePath = Path.Combine("files", "fotosPublicaciones", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }

        public  void actualizarPostulaciones()
        {
        

           

            List<Publicacion> publicaciones = context.Publicaciones.Where(o=>o.estado==0).ToList();
           
            foreach (Publicacion pub in publicaciones)
            {

                

                DateTime fechaActual = DateTime.Now;
                if (fechaActual>= pub.limitePostulaciones)
                {


                    List<Postulacion> postulaciones = context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == pub.id).ToList();

                    int maxLikes = postulaciones.Max(o => o.likesPostulacion.Count());



                    postulaciones = postulaciones.Where(o => o.likesPostulacion.Count() == maxLikes).ToList();

                    DateTime fechaMin = postulaciones.Min(o => o.fechaCreacion);

                    Postulacion postulacion = postulaciones.FirstOrDefault(o => o.fechaCreacion == fechaMin);

                    //logica para cambiar de estados segun situacion de publicaion y postulacion
                    Publicacion publicacion = context.Publicaciones.FirstOrDefault(o => o.id == pub.id);


                    List<Donacion> donaciones = context.Donaciones.Where(o => o.idPublicacion == pub.id).ToList();
                    Double sumaDonaciones = donaciones.Sum(o => o.monto);

                    Double montoRestante = 0;
                    if (sumaDonaciones >= (Double)postulacion.monto)
                    {
                        montoRestante = sumaDonaciones - (Double)postulacion.monto;

                        //ya tiene monto completo y trabajador asignado
                        publicacion.estado = 3;
                        publicacion.montoAsignado = (Double)postulacion.monto;
                        postulacion.estado = 2;
                        //envio de dinero restante a caja
                        Caja caja = context.Cajas.FirstOrDefault();
                        caja.monto += montoRestante;
                    }
                    if (sumaDonaciones < (Double)postulacion.monto)
                    {
                        publicacion.estado = 2;
                        publicacion.montoAsignado = (Double)postulacion.monto;
                        postulacion.estado = 1;

                    }


                    context.SaveChanges();

                    List<Postulacion> postulacionesRemover = context.Postulaciones.Include("likesPostulacion").Where(o => o.idPublicacion == pub.id && o.estado == 0).ToList();

                    //guardar lista
                    List<Postulacion> posts = new List<Postulacion>();
                    posts.AddRange(postulacionesRemover);

                    context.Postulaciones.RemoveRange(postulacionesRemover);

                    context.SaveChanges();


                }






            }


        }
    }
    
}
