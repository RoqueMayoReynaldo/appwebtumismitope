﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class LikePostulacion
    {
        public int id { get; set; }
        public int idPostulacion { get; set; }
        public int idUsuario { get; set; }
        public DateTime fechaLike { get; set; }

    }

}
