﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class Publicacion
    {
        public int id { get; set; }
        public string descripcion { get; set; }
        public int estado { get; set; }
        public int vista { get; set; }
        public string titulo { get; set; }
        public DateTime fechaCreacion { get; set; }
        public DateTime limitePostulaciones { get; set; }
        public int idUsuario { get; set; }
        public Usuario usuario { get; set; }

        public Double montoAsignado { get; set; }
        public List<Postulacion> postulaciones {get;set;}
        public List<Donacion> donaciones { get; set; }
        public List<ReaccionPublicacion> reacciones { get; set; }
        public List<Comentario> comentarios { get; set; }
        public List<Foto> fotos { get; set; }
    }
}
