﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTUMISMITOPE.Models
{
    public class FotoEvidencia
    {
        public int id { get; set; }
        public String contenido { get; set; }
        public int idEvidencia { get; set; }

    }
}
